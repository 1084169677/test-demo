import React ,{useState} from 'react'
// import ReactDOM from 'react-dom';
import {withRouter} from 'react-router-dom';
import 'antd/dist/antd.css';
import './LayoutView.css';
import { 
  Layout, Menu,Input,Button, BackTop
} from 'antd';
// import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';
import { Routes } from '../../routes';
import MenuItem from 'antd/lib/menu/MenuItem';

const { Header, Content ,Footer} = Layout;
const routes = Routes.filter(route=>route.isShow);

const list =['123','开心','甘雨','飘柔']

//查询
function handleSerach(keyWord){//搜索
  var reg = new RegExp(keyWord);
  var resultCode = '0';
  for (let i = 0; i < list.length; i++) {
    if (reg.test(list[i])) {
      resultCode = '1'
    }
  }
  console.log(resultCode)
}

//layout
function LayoutView(props) {
  const [query, setquery] = useState('');
  const [scrollTop, setscrollTop] = useState(0);
  window.onscroll=()=>{
    setscrollTop({ scrollTop : document.documentElement.scrollTop})
  }
  var path = props.location.pathname
    return (
      <div>
        <Layout>
          <div>
            <Header 
              className={scrollTop.scrollTop >100 ? 'header-layout-second':'header-layout-first'}
              // style={{ position: 'fixed', zIndex: 1, width: '100%',backgroundColor:'#93B874',color:'#475347',height:'72px'}}
            >
            <div className="header" >
              <div className={scrollTop.scrollTop >100 ? 'header-logo-second':'header-logo'}>
                {/* logo */}
                <span style={{float:"left"}}><a href='#admin/home'>logo</a></span>
                {/* 搜索栏 */}
                <span style={{display:"inline-block"}}>
                  <span style={{float:"left"}}>
                  <Input 
                    className="header-logo-search"
                    placeholder="Find Anything . . ."
                    onChange={(e)=>setquery({ query : e.target.value})}
                    onPressEnter={(e)=>handleSerach(e.target.value)}
                  />
                  </span>
                  <span style={{float:"left"}}> 
                    <Button type="primary"
                      onClick={()=>handleSerach(query.query)}
                    >
                      <svg t="1610349152383" className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="1214" width="20" height="20"><path d="M946.222 844.994 727.729 624.668c-2.543-2.549-5.353-4.496-8.036-6.617 36.908-57.103 58.459-125.121 58.459-198.387 0-201.282-161.79-364.428-361.498-364.428-199.596 0-361.498 163.146-361.498 364.428 0 201.31 161.902 364.42 361.498 364.42 72.648 0 140.196-21.784 196.926-58.999 2.066 2.768 3.961 5.521 6.463 8.05l218.521 220.353c14.919 14.997 34.35 22.472 53.809 22.472 19.472 0 38.916-7.474 53.822-22.436C975.879 923.528 975.879 874.967 946.222 844.994M416.654 669.007c-136.347 0-247.334-111.872-247.334-249.343 0-137.45 110.988-249.351 247.334-249.351s247.334 111.901 247.334 249.351C663.989 557.134 553 669.007 416.654 669.007" p-id="1215" fill="#ffffff"></path></svg>
                    </Button>
                  </span>
                  {/* <Search placeholder="Find Anything" onSearch={value => console.log(value)} enterButton /> */}
                </span>    
              </div>
              <div className="header-menu">
                <Menu
                  theme="dark"
                  mode="horizontal"
                  className={scrollTop.scrollTop >100 ?'header-Menu-second':'header-Menu-first'}
                  // style={{ lineHeight:'72px',textAlign:'right',backgroundColor:'#93B874',overflow:"visible" }}
                >
                  {routes.map(route=>{
                    return (
                      <MenuItem 
                        key={route.path} 
                        onClick={e=>props.history.push(e.key)}
                        className={scrollTop.scrollTop >100?'header-Menu-MenuItem-second':'header-Menu-MenuItem-first'}
                        // style={{color:"#475347",fontSize:'10px',width:'72px'}}
                      >
                        <div 
                          className={scrollTop.scrollTop >100 ?'header-MenuItem-li-second':'header-MenuItem-li-first'}
                          // style={{textAlign:'center',height:'72px'}}
                        >
                          {
                            document.documentElement.scrollTop <= 100 ?<div style={{maxHeight:'20px' }}> 
                              {route.Icon}
                            </div>:null
                          }
                          <div style={{height:'39px'}}>
                            {route.title}
                          </div>
                        </div>
                      </MenuItem>
                      )
                  })}
                </Menu>
              </div>
            </div>
          </Header>
          </div>
          {
            path === "/admin/home" ?<div className='home-back'>
            </div> :null
          }
          <div className={path === "/admin/home" ?'homecontent':'content'}>
            <Layout >
              <Content className="content-children"
                style={{
                  backgroundColor:'#fff',
                  padding:'24px',
                  minHeight:'620px'
                }}
              >
                {props.children}
              </Content>
            </Layout>
          </div>
          <div className="footer">
            <Footer className="footer-nav">
              <p>zcmuzcmuzcmuzcmu</p>
            </Footer>
            <Footer className="footer-foot">
              zcmu
            </Footer>
          </div>
          <BackTop 
            visibilityHeight="300"
          />
        </Layout>
      </div>
    )
}

export default withRouter(LayoutView)

import React, { Component } from 'react'
import {
  Button
} from 'antd';


class applicationView extends Component {
  constructor(props,context){
    super(props,context);
    this.state = { }; 
  }
  // 返回首页
  BackHome() {
    this.props.history.push('/admin/home')
  }
  render() {
    return (
      <div>
        <p>认证申请</p>
        <Button type="primary" onClick={() => this.BackHome()}>返回首页</Button>
      </div >
    );
  }
}

export default applicationView;
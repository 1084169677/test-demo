import React, { Component } from 'react';
import { Button} from 'antd';

class PageNotFound extends Component {
   // 返回首页
   BackHome() {
    this.props.history.push('/admin/home')
   }
  render() {
    return (
        <div >
          <div >
            {/* <img src={`${CDN_BASE}/assets/imgs/404 icon.png`}/> */}
            <p>您访问的页面不存在</p>
            <Button type="primary" onClick={() => this.BackHome()}>返回首页</Button>
          </div>
      </div>
    )
  }
}

export default PageNotFound


import React, { Component } from 'react';
import {
  Button 
} from 'antd';

class RegisterView extends Component {
  constructor(props,context){
    super(props,context);
    this.state = { }; 
  }

  // 返回首页
  BackHome() {
    // window.location.replace('#admin/home')
    this.props.history.push('/admin/home')
  }
  
  render(){
      return (
        <div className="News">
          <p>立即注册</p>
          <div>
            <Button type="primary" onClick={() => this.BackHome()}>取消</Button>
          </div>
        </div>
      )
  }
}
export default RegisterView;

import React, { Component } from 'react'
import { Form, Input, Button, Checkbox } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import './LoginView.css';
class Login extends Component {
  constructor(props,context){
    super(props,context);
    this.state = { }; 
  }
  // 返回首页
  BackHome() {
    this.props.history.push('/admin/home')
  }
  //提交登陆
  onLogin = (values) => {
    console.log('Received values of form: ', values);
  }

  render() {
    return (
      <div>
        <div style={{width:'200px',height:'300px'}}>
          <Form
            name="normal_login"
            className="login-form"
            initialValues={{
              remember: true,
            }}
            onFinish={this.onLogin}
          >
            <Form.Item
              name="username"
              rules={[
                {
                  required: true,
                  message: '请输入工号!',
                },
              ]}
            >
              <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: '请输入密码!',
                },
              ]}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Password"
              />
            </Form.Item>
            <Form.Item>
              <Form.Item name="remember" valuePropName="checked" noStyle>
                <Checkbox>记住我</Checkbox>
              </Form.Item>

              <a className="login-form-forgot" href="">
                忘记密码
              </a>
            </Form.Item>

            <Form.Item>
              <Button type="primary" htmlType="submit" className="login-form-button">
                登陆
              </Button>
              <a href="#/Register">立即注册!</a>
            </Form.Item>
          </Form>
        </div>
        <div>
          <Button type="primary" onClick={() => this.BackHome()}>返回首页</Button>
        </div>
      </div>
      
    );
  }
}


export default Login;
import React, { Component } from 'react';
import './HomeView.css';
import {
  Button ,Card
} from 'antd';
const { Meta } = Card;

class HomeView extends Component {
  constructor(props,context){
    super(props,context);
    this.state = { }; 
  }

  render() {
    return (
      <div>
        <section>
          <div className="navBar">
            <h3>导航分类</h3>
            <div className="navBar-content">
              <ul>
                <li>
                  <Button style={{backgroundColor:'#e77'}}
                    onClick={()=>window.location.href="#admin/SchoolMsg"}
                  >
                    <p>校园活动信息</p>
                    <p>热点信息</p>
                  </Button>
                </li>
                <li>
                  <Button style={{backgroundColor:'#ee7'}}
                    onClick={()=>window.location.href="#admin/PartTime"}
                  >
                    <p>兼职信息</p>
                    <p>热点信息</p>
                  </Button>
                </li>
                <li>
                  <Button style={{backgroundColor:'#e7e'}}
                    onClick={()=>window.location.href="#admin/ShopStore"}
                  >
                    <p>商品信息</p>
                    <p>热点信息</p>
                  </Button>
                </li>
                <li>
                  <Button style={{backgroundColor:'#7e3'}}
                    onClick={()=>window.location.href="#admin/HelpPost"}
                  >
                    <p>互助帖子</p>
                    <p>热点信息</p>
                  </Button>
                </li>
                <li>
                  <Button style={{backgroundColor:'#4e2'}}
                    onClick={()=>window.location.href="#admin/Competition"}
                  >
                    <p>学科竞赛信息</p>
                    <p>热点信息</p>
                  </Button>
                </li>
                <li>
                  <Button style={{backgroundColor:'#e12'}}
                    onClick={()=>window.location.href="#admin/SchoolHotMsg"}
                  >
                    <p>校园热点</p>
                    <p>热点信息</p>
                  </Button>
                </li>
              </ul>
            </div>
          </div>
          <div className="hotMes">
            <h3>热点信息浏览</h3>
            <div className="hotMes-content" style={{width:"100%",minHeight:'500px'}}>
              <div className="hotMes-content-card" style={{float:'left' ,width:'70%'}}>
                <Card
                  hoverable
                  style={{ width:'30%',float:'left' }}
                  cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
                >
                <Meta title="Europe Street beat" description="www.instagram.com" />
                </Card>
                <Card
                  hoverable
                  style={{ width:'30%',float:'left',marginLeft:'3%' }}
                  cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
                >
                <Meta title="Europe Street beat" description="www.instagram.com" />
                </Card>
                <Card
                  hoverable
                  style={{ width:'30%',float:'left',marginLeft:'3%' }}
                  cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
                >
                <Meta title="Europe Street beat" description="www.instagram.com" />
                </Card>
              </div>
              <div className="hotMes-content-nav" style={{float:'left',width:'30%'}}>
                <Card
                    hoverable
                    style={{ width: 240 }}
                    cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
                  >
                  <Meta title="Europe Street beat" description="www.instagram.com" />
                  </Card>
              </div>  
            </div>
          </div>
          <div className="newMes" style={{clear:'both',marginTop:50}}>
            <div className="newMes-head">
              <h3 style={{float:"left"}}>新信息</h3>
              <span style={{position:'relative',right:'-80%'}}>
                {/* 跳转最新信息页面 */}
                <a style={{color:'#93B874'}} href='#admin/News'>
                <p style={{float:'left',margin:0,fontSize:'17px'}}>查看更多</p>
                <svg t="1610607180135" className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="1110" data-spm-anchor-id="a313x.7781069.0.i4" width="25" height="25"><path d="M357.376 852.309333a25.6 25.6 0 0 0 36.181333 36.181334l358.4-358.4a25.6 25.6 0 0 0 0-36.181334l-358.4-358.4a25.6 25.6 0 0 0-36.181333 36.181334L697.685333 512l-340.309333 340.309333z" p-id="1111" fill="#93B874"></path></svg>
                </a>
              </span>
            </div>
            <div className="newMes-content" style={{width:"100%",minHeight:'500px',clear:'both'}}>
              <Card
                hoverable
                style={{ width:'20%' }}
                cover={<img alt="example" src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" />}
              >
                <Meta title="Europe Street beat" description="www.instagram.com" />
              </Card>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default HomeView;
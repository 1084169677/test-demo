import home from "../pages/home/HomeView";
import pubInformationView from "../pages/pubInformation/pubInformationView"; //发布信息
import applicationView from "../pages/application/applicationView";  //认证申请
import aboutUsView from "../pages/aboutUs/aboutUsView"//关于我们
import mineView from "../pages/mine/mineView"//我的
import Login from "../pages/Login/LoginView"; //登陆页面
import Register from "../pages/Register/RegisterView"//注册页面
import PageNotFound from "../pages/PageNotFound/PageNotFoundView"; //404
import News from "../pages/Information/NewInformation/NewInformationView"; //新信息
import PartInformation from "../pages/Information/PartInformation/PartInformationView" //兼职信息
import SchoolInformation from "../pages/Information/SchoolInformation/SchoolInformationView" //校园信息
import Shop from "../pages/Information/ShopInformation/ShopInformationView";//商品信息
import Competition from "../pages/Information/CompetitionInformation/CompetitionInformationView";//学科竞赛
import HelpPost from "../pages/Information/HelpingPost/HelpingPostView";//互助帖子
import HotSchoolMsg from "../pages/Information/HotSchoolMsg/HotSchoolMsgView";//校园热点
import { LaptopOutlined, NotificationOutlined } from '@ant-design/icons';

// 没有导航栏路由
export const mainRoutes = [
    {
        path:'/Login',
        component:Login
    },
    {
        path:'/Register',
        component:Register
    },
]
//导航栏下路由
export const Routes = [ //layout路由
    //不展示在导航栏上的路由
    {
<<<<<<< HEAD
=======
        path:'/admin/404',
        component:PageNotFound,
        title:'404',
        isShow:false,
    },
    {
        path:'/admin/News',
        component:News,
        title:'新信息News！',
        isShow:false,
    },
    {
        path:'/admin/PartTime',
        component:PartInformation,
        title:'兼职信息',
        isShow:false,
    },
    {
        path:'/admin/SchoolMsg',
        component:SchoolInformation,
        title:'学校信息',
        isShow:false,
    },
    {
        path:'/admin/ShopStore',
        component:Shop,
        title:'商品信息',
        isShow:false,
    },
    {
        path:'/admin/Competition',
        component:Competition,
        title:'学科竞赛信息',
        isShow:false,
    },
    {
        path:'/admin/HelpPost',
        component:HelpPost,
        title:'互助帖子',
        isShow:false,
    },
    {
        path:'/admin/SchoolHotMsg',
        component:HotSchoolMsg,
        title:'校园热点',
        isShow:false,
    },
    //展示在导航栏上的路由
    {
>>>>>>> d86f0048ccac5f50f85de312cb41169628870c88
        path:'/admin/home',
        component:home,
        title:'首页',
        Icon:<NotificationOutlined/>,
        isShow:true,
        exact:true
    },
    {
        path:'/admin/404',
        component:PageNotFound,
        title:'404',
        isShow:false,
    },
    {
        path:'/admin/pubInformation',
        component:pubInformationView,
        title:'发布信息',
        Icon:<NotificationOutlined/>,
        isShow:true,
        exact:true
    },
    {
        path:'/admin/application',
        isShow:true,
        title:'认证申请',
        Icon:<LaptopOutlined/>,
        component:applicationView
    },
    {
        path:'/admin/aboutUs',
        isShow:true,
        title:'关于我们',
        Icon:<LaptopOutlined/>,
        component:aboutUsView
    },
    {
        path:'/admin/mine',
        isShow:true,
        title:'我的',
        Icon:<LaptopOutlined/>,
        component:mineView
    },
]
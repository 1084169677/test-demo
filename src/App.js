import React from 'react';
import { Switch,Route,Redirect } from 'react-router-dom'
import{ Routes } from './routes' 
import MyLayout from './components/layout/LayoutView'
import './App.css'
function App() {
  return (
    <MyLayout>
        <Switch>
        {
            Routes.map(route=>{
                // console.log(route)
                return (
                    <Route 
                        key={route.path} 
                        path={route.path} 
                        exact={route.exact} 
                        render={routeProps=>{
                            return <route.component {...routeProps}/>
                        }}
                    />
                );
            })
        }
        <Redirect to="/admin/404"/>
        </Switch>
    </MyLayout>
  );
}

export default App;
